import * as AWS from 'aws-sdk';
import { IExecutionPlan } from '../src/interfaces/execution-plan';
import { v4 as uuidv4 } from 'uuid';

AWS.config.update({ region: 'eu-central-1' });

const generateEvents = async () => {
  const eventbridge = new AWS.EventBridge();
  const numberOfEvents = 1;
  const numberOfEntriesPerEvent = 1;
  let entries = [];
  let eventBridgeEvent;
  let executionPlan: IExecutionPlan;
  let id;
  let customerId;
  for (let i = 0; i < numberOfEvents; i++) {
    entries = [];
    console.log(i);
    for (let j = 0; j < numberOfEntriesPerEvent; j++) {
      id = uuidv4();
      console.log(id);
      customerId = uuidv4();
      executionPlan = {
        id: `${id}`,
        customerId: `${customerId}`,
      };

      entries.push({
        DetailType: 'Execution Plan Created',
        EventBusName: 'emp-bus',
        Source: 'ssr',
        Time: new Date(),
        Detail: JSON.stringify(executionPlan),
      });
    }

    eventBridgeEvent = {
      Entries: entries,
    };
    await eventbridge.putEvents(eventBridgeEvent).promise();
  }
};

void generateEvents();
