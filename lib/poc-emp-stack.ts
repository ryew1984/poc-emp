import * as cdk from '@aws-cdk/core';
import { EventBus, Rule, RuleTargetInput, EventField } from '@aws-cdk/aws-events';
import { Queue } from '@aws-cdk/aws-sqs';
import { Table, AttributeType, BillingMode } from '@aws-cdk/aws-dynamodb';
import { Function, Runtime, Code } from '@aws-cdk/aws-lambda';
import { SqsEventSource } from '@aws-cdk/aws-lambda-event-sources';
import * as targets from '@aws-cdk/aws-events-targets';
import * as apigw from '@aws-cdk/aws-apigateway';
import * as iam from '@aws-cdk/aws-iam';
// import * as destinations from '@aws-cdk/aws-lambda-destinations';

export class PocEmpStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // eventbridge
    const empBus: EventBus = new EventBus(this, 'empBus', {
      eventBusName: 'emp-bus',
    });

    // eventbridge rule
    const ssrReceivedRule: Rule = new Rule(this, 'ssrExecutionPlanCreatedRule', {
      ruleName: 'ssr-execution-plan-created-rule',
      description: 'Rule matching ssr execution plan created',
      eventBus: empBus,
      eventPattern: {
        source: ['ssr'],
        detailType: ['Execution Plan Created'],
      },
    });

    // sqsQueue
    const ssrQueue: Queue = new Queue(this, 'ssrQueue', {
      queueName: 'ssr-queue',
    });

    // dynamoDB
    const executionPlanTable: Table = new Table(this, 'ssrExecutionPlan', {
      partitionKey: { name: 'id', type: AttributeType.STRING },
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      billingMode: BillingMode.PAY_PER_REQUEST,
    });

    // lambda
    const executionPlanSentHandler = new Function(this, 'ExecutionPlanSentHandler', {
      code: Code.fromAsset('src'),
      reservedConcurrentExecutions: 500,
      memorySize: 128,
      handler: 'execution-plan-sent-handler.handler',
      runtime: Runtime.NODEJS_12_X,
      timeout: cdk.Duration.seconds(2),
      environment: {
        QUEUE_URL: ssrQueue.queueUrl,
        TABLE_NAME: executionPlanTable.tableName,
      },
      events: [
        new SqsEventSource(ssrQueue, {
          batchSize: 10,
        }),
      ],
    });

    // eventbridge target
    ssrReceivedRule.addTarget(
      new targets.SqsQueue(ssrQueue, {
        message: RuleTargetInput.fromObject({
          id: EventField.fromPath('$.detail.id'),
          customerId: EventField.fromPath('$.detail.customerId'),
        }),
      }),
    );

    const testSsrExecutionPlanReceived = new Function(this, 'testSsrExecutionPlanReceived', {
      runtime: Runtime.NODEJS_12_X,
      code: Code.fromAsset('src'),
      handler: 'test-ssr-execution-plan-received-handler.handler',
      environment: {
        QUEUE_URL: ssrQueue.queueUrl,
        TABLE_NAME: executionPlanTable.tableName,
      },
    });

    const eventPolicy = new iam.PolicyStatement({
      effect: iam.Effect.ALLOW,
      resources: ['*'],
      actions: ['events:PutEvents'],
    });

    testSsrExecutionPlanReceived.addToRolePolicy(eventPolicy);

    new apigw.LambdaRestApi(this, 'Endpoint', {
      handler: testSsrExecutionPlanReceived,
    });

    executionPlanTable.grant(executionPlanSentHandler, 'dynamodb:PutItem');

    executionPlanTable.grant(executionPlanSentHandler, 'dynamodb:BatchWriteItem');

    executionPlanTable.grant(testSsrExecutionPlanReceived, 'dynamodb:GetItem');

    // const ssrExecutionPlanGenerator = new Function(this, 'ssrExecutionPlanGenerator', {
    //   runtime: Runtime.NODEJS_12_X,
    //   code: Code.fromAsset('src'),
    //   timeout: cdk.Duration.minutes(15),
    //   handler: 'ssr-execution-plan-generator-handler.handler',
    // });

    // const ssrGenerateExecutionPlanRule: Rule = new Rule(this, 'ssrGenerateDummyExecutionPlanRule', {
    //   ruleName: 'ssr-generate-dummy-execution-plan-rule',
    //   description: 'Rule matching generate ssr execution plan requested',
    //   eventBus: empBus,
    //   eventPattern: {
    //     source: ['ssr'],
    //     detailType: ['Execution Plan Generate Requested'],
    //   },
    // });

    // ssrGenerateExecutionPlanRule.addTarget(new targets.LambdaFunction(ssrExecutionPlanGenerator));

    // ssrExecutionPlanGenerator.addToRolePolicy(eventPolicy);
  }
}
