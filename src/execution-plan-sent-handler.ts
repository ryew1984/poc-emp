import { DynamoDB } from 'aws-sdk';
import { env } from 'process';
import { SQSEvent, SQSHandler } from 'aws-lambda';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { IExecutionPlan } from './interfaces/execution-plan';

export const handler: SQSHandler = (event: SQSEvent) => {
  const tableName: string = env.TABLE_NAME || '';
  const dynomoDBClient: DocumentClient = new DynamoDB.DocumentClient();
  let executionPlan: IExecutionPlan;
  const putEvents: { PutRequest: { Item: { id: string; customerId: string } } }[] = [];
  event.Records.forEach((record) => {
    executionPlan = JSON.parse(record.body);
    console.log(executionPlan);
    console.log(executionPlan.id);
    console.log(executionPlan.customerId);
    putEvents.push({
      PutRequest: {
        Item: {
          id: executionPlan.id,
          customerId: executionPlan.customerId,
        },
      },
    });
  });

  const params = {
    RequestItems: {
      [tableName]: putEvents,
    },
  };

  console.log(params);

  dynomoDBClient.batchWrite(params, function (err, data) {
    if (err) {
      console.log('Error', err);
    } else {
      console.log('Success', data);
    }
  });
};
