import { Handler } from 'aws-lambda';
import * as AWS from 'aws-sdk';
import { IExecutionPlan } from './interfaces/execution-plan';

export const handler: Handler = async (event, context) => {
  console.log(event);
  console.log(context);

  const eventbridge = new AWS.EventBridge();
  const numberOfEvents = 100;
  const numberOfEntries = 10;
  let entries = [];
  let eventBridgeEvent;
  let executionPlan: IExecutionPlan;
  for (let i = 0; i < numberOfEvents; i++) {
    entries = [];
    for (let j = 0; j < numberOfEntries; j++) {
      executionPlan = {
        id: `${i}-id-${j}`,
        customerId: `${i}-cid-${j}`,
      };

      entries.push({
        DetailType: 'Execution Plan Created',
        EventBusName: 'emp-bus',
        Source: 'ssr',
        Time: new Date(),
        Detail: JSON.stringify(executionPlan),
      });
    }

    eventBridgeEvent = {
      Entries: entries,
    };
    await eventbridge.putEvents(eventBridgeEvent).promise();
  }
};
