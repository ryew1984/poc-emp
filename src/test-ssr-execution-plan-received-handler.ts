import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult } from 'aws-lambda';
import * as AWS from 'aws-sdk';
import { DynamoDB } from 'aws-sdk';
import { env } from 'process';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { IExecutionPlan } from './interfaces/execution-plan';

let testResult: string;
let statusCode: number;

export const handler: APIGatewayProxyHandler = async (
  event: APIGatewayProxyEvent,
  context,
): Promise<APIGatewayProxyResult> => {
  const tableName: string = env.TABLE_NAME || '';
  const dynomoDBClient: DocumentClient = new DynamoDB.DocumentClient();

  const eventbridge = new AWS.EventBridge();

  const executionPlan: IExecutionPlan = {
    id: context.awsRequestId,
    customerId: context.awsRequestId,
  };

  const eventBridgeEvent = {
    Entries: [
      {
        DetailType: 'Execution Plan Created',
        EventBusName: 'emp-bus',
        Source: 'ssr',
        Detail: JSON.stringify(executionPlan),
      },
    ],
  };

  await eventbridge.putEvents(eventBridgeEvent).promise();

  setTimeout(() => console.log('Proceeding to query table'), 500);

  const params = {
    TableName: tableName,
    Key: {
      id: context.awsRequestId,
    },
  };

  dynomoDBClient.get(params, function (err, data) {
    if (err) {
      testResult = `Unable to read item. Error JSON:${JSON.stringify(err, null, 2)}`;
      statusCode = 400;
    } else {
      testResult = `GetItem succeeded:${JSON.stringify(data, null, 2)}`;
      statusCode = 200;
    }
  });

  return {
    statusCode: statusCode,
    headers: {
      'Content-Type': 'text/html',
    },
    body: testResult,
  };
};
