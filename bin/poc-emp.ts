#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { PocEmpStack } from '../lib/poc-emp-stack';

const app = new cdk.App();
new PocEmpStack(app, 'PocEmpStack');
