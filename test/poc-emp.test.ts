import { expect as expectCDK, haveResource } from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import * as PocEmp from '../lib/poc-emp-stack';

test('Required resources are present', () => {
  const app = new cdk.App();
  // WHEN
  const stack = new PocEmp.PocEmpStack(app, 'MyTestStack');
  // THEN
  expectCDK(stack).to(haveResource('AWS::Events::EventBus'));
  expectCDK(stack).to(haveResource('AWS::DynamoDB::Table'));
  expectCDK(stack).to(haveResource('AWS::SQS::Queue'));
  expectCDK(stack).to(haveResource('AWS::Events::Rule'));
  expectCDK(stack).to(haveResource('AWS::Lambda::Function'));
});

test('Lambda configured correctly', () => {
  const app = new cdk.App();

  const stack = new PocEmp.PocEmpStack(app, 'MyTestStack');

  expectCDK(stack).to(
    haveResource('AWS::Lambda::Function', {
      MemorySize: 128,
      ReservedConcurrentExecutions: 500,
    }),
  );
});
