# POC EMP

* Infrastructure of the POC EMP: eventbus -> sqs -> lambda -> dynamodb
* A execution plan event generator is located in tools
* Load test with performance of 20k events / minute
* Rough estimate of 5 million events using this design is here: https://calculator.aws/#/estimate?id=e2f603092cade6c4cbc931c210a1398577d5b3df
* A test lambda is included that generates an event and retrieves it through dynamodb

## Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template
